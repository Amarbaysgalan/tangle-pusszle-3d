﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshAss : MonoBehaviour
{
    private SkinnedMeshRenderer _meshRenderer;

    private MeshCollider _collider;
    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<MeshCollider>();
        _meshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        _collider.sharedMesh = _meshRenderer.sharedMesh;
    }
}
